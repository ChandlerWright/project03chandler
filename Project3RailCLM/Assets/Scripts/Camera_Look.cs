﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Code provided in the Tele-Portal games for smooth looking
//with edits to correct getting stuck up or down
[AddComponentMenu("Camera-Control/Smooth Mouse Look")]
public class Camera_Look : MonoBehaviour
{

    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

	public bool isLocked;

    float rotationX = 0F;
    float rotationY = 0F;

    private List<float> rotArrayX = new List<float>();
    float rotAverageX = 0F;

    private List<float> rotArrayY = new List<float>();
    float rotAverageY = 0F;

    public float frameCounter = 20;

    Quaternion originalRotation;

    void Update()
    {
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if (Cursor.visible == true) {
				Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Locked;
			} else if (Cursor.visible == false) 
			{
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
			}
		}

		if (isLocked == false) {
			if (axes == RotationAxes.MouseXAndY) {
				rotAverageY = 0f;
				rotAverageX = 0f;

				rotationY += Input.GetAxis ("Mouse Y") * sensitivityY;
				rotationX += Input.GetAxis ("Mouse X") * sensitivityX;

				/** Edits to correct camera*/
				//Lock rotation at the top of viewing angle if gone over
				if (rotationY > maximumY)
					rotationY = maximumY;

				//Lock rotation at the bottom of viewing angle if gone under
				if (rotationY < minimumY)
					rotationY = minimumY;

				rotArrayY.Add (rotationY);
				rotArrayX.Add (rotationX);

				if (rotArrayY.Count >= frameCounter) {
					rotArrayY.RemoveAt (0);
				}
				if (rotArrayX.Count >= frameCounter) {
					rotArrayX.RemoveAt (0);
				}

				for (int j = 0; j < rotArrayY.Count; j++) {
					rotAverageY += rotArrayY [j];
				}
				for (int i = 0; i < rotArrayX.Count; i++) {
					rotAverageX += rotArrayX [i];
				}

				rotAverageY /= rotArrayY.Count;
				rotAverageX /= rotArrayX.Count;

				rotAverageY = ClampAngle (rotAverageY, minimumY, maximumY);
				rotAverageX = ClampAngle (rotAverageX, minimumX, maximumX);

				Quaternion yQuaternion = Quaternion.AngleAxis (rotAverageY, Vector3.left);
				Quaternion xQuaternion = Quaternion.AngleAxis (rotAverageX, Vector3.up);

				transform.localRotation = originalRotation * xQuaternion * yQuaternion;
			} else if (axes == RotationAxes.MouseX) {
				rotAverageX = 0f;

				rotationX += Input.GetAxis ("Mouse X") * sensitivityX;

				rotArrayX.Add (rotationX);

				if (rotArrayX.Count >= frameCounter) {
					rotArrayX.RemoveAt (0);
				}
				for (int i = 0; i < rotArrayX.Count; i++) {
					rotAverageX += rotArrayX [i];
				}
				rotAverageX /= rotArrayX.Count;

				rotAverageX = ClampAngle (rotAverageX, minimumX, maximumX);

				Quaternion xQuaternion = Quaternion.AngleAxis (rotAverageX, Vector3.up);
				transform.localRotation = originalRotation * xQuaternion;
			} else {
				rotAverageY = 0f;

				rotationY += Input.GetAxis ("Mouse Y") * sensitivityY;

            

				rotArrayY.Add (rotationY);

				if (rotArrayY.Count >= frameCounter) {
					rotArrayY.RemoveAt (0);
				}
				for (int j = 0; j < rotArrayY.Count; j++) {
					rotAverageY += rotArrayY [j];
				}
				rotAverageY /= rotArrayY.Count;

				rotAverageY = ClampAngle (rotAverageY, minimumY, maximumY);

				Quaternion yQuaternion = Quaternion.AngleAxis (rotAverageY, Vector3.left);
				transform.localRotation = originalRotation * yQuaternion;
			}
		}
    }

    void Start()
    {
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
        Rigidbody rb = GetComponent<Rigidbody>();
        if (rb)
            rb.freezeRotation = true;
        originalRotation = transform.localRotation;
		isLocked = false;
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }
            if (angle > 360F)
            {
                angle -= 360F;
            }
        }
        return Mathf.Clamp(angle, min, max);
    }
}