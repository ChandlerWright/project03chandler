﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Shake : MonoBehaviour {

	public float timeOfShake;
	private float timeLeft;
	public GameObject playerCamera;
	public Vector3 originalPosition;

	// Use this for initialization
	void Start () {
		playerCamera = Camera.main.gameObject;
		originalPosition = Camera.main.transform.position;
		timeLeft = timeOfShake;
	}
	
	// Update is called once per frame
	void Update () {
		CameraShaking ();
	}

	public void CameraShaking(){
		if (timeLeft > 0) {
			playerCamera.transform.localPosition = originalPosition + (Random.insideUnitSphere / 2);
			timeLeft -= (1 * Time.deltaTime);
		} else if (timeLeft < 0) {
			timeLeft = 0;
			playerCamera.transform.position = originalPosition;
		}
	}
}
